package com.polahniuk.view;

import com.polahniuk.model.*;
import com.polahniuk.model.bouquet.*;
import com.polahniuk.model.bouquet.impl.BouquetImpl;
import com.polahniuk.model.bouquet.state.Order;
import com.polahniuk.model.decoration.BouquetDecorator;
import com.polahniuk.model.decoration.impl.Delivery;
import com.polahniuk.model.decoration.impl.Package;
import com.polahniuk.model.facade.BouquetOperation;
import com.polahniuk.model.flower.Color;
import com.polahniuk.model.flower.Flower;
import com.polahniuk.model.flower.FlowerType;
import com.polahniuk.model.flower.impl.Camomile;
import com.polahniuk.model.flower.impl.Chrysanthemum;
import com.polahniuk.model.flower.impl.Rose;
import com.polahniuk.model.flower.impl.Tulip;
import org.apache.logging.log4j.*;

import java.util.*;

/**
 * Main view of project. Show menu in console.
 */
public class Menu {

    private Logger log = LogManager.getLogger(Menu.class);
    private Scanner sc = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methods;
    private BouquetOperation bouquetOperation;

    /**
     * Launch methods due to interface{@link Printable}.
     */
    public Menu() {
        bouquetOperation = new BouquetOperation();
        setMenu();
        methods = new LinkedHashMap<>();
        methods.put("1", this::catalog);
        methods.put("2", this::customBouquet);
        methods.put("3", this::decorate);
        methods.put("5", this::orderState);
        show();
    }

    private void catalog() {
        System.out.println("Bouquet catalog:");
        for (int i = 0; i < BouquetType.values().length; i++) {
            System.out.println("   " + (i + 1) + BouquetType.values()[i]);
        }
        System.out.println("Choose you want:");
        int index = sc.nextInt() - 1;
        System.out.println("Choose size:");
        for (int i = 0; i < BouquetSize.values().length; i++) {
            System.out.println("   " + (i + 1) + BouquetSize.values()[i]);
        }
        int size = sc.nextInt() - 1;
        if (index >= 0 && index < BouquetType.values().length) {
            bouquetOperation.addBouquet(index, size);
        }
    }

    private void customBouquet() {
        Bouquet bouquet = new BouquetImpl(BouquetType.CUSTOM);
        Flower flower;
        Color color;
        System.out.println("Choose flower:");
        for (int i = 0; i < FlowerType.values().length; i++) {
            System.out.println("   " + (i + 1) + FlowerType.values()[i]);
        }
        System.out.println("Choose you want:");
        int index = sc.nextInt() - 1;
        for (int i = 0; i < Color.values().length; i++) {
            System.out.println("   " + (i + 1) + Color.values()[i]);
        }
        System.out.println("Choose color:");
        int colorIndex = sc.nextInt() - 1;
        System.out.println("Enter quantity:");
        int quantity = sc.nextInt();
        if (index == 0) {
            flower = new Camomile(Color.values()[colorIndex]);
        } else if (index == 1) {
            flower = new Chrysanthemum(Color.values()[colorIndex]);
        } else if (index == 2) {
            flower = new Rose(Color.values()[colorIndex]);
        } else if (index == 3) {
            flower = new Tulip(Color.values()[colorIndex]);
        } else {
            flower = null;
        }
        bouquet.addFlowers(flower, quantity);
        bouquetOperation.addBouquet(bouquet);
    }

    private void decorate() {
        Bouquet bouquet;
        DeliveryType deliveryType;
        PackageType packageType;


        for (int i = 0; i < bouquetOperation.getBouquets().size(); i++) {
            System.out.println("   " + (i +1) + bouquetOperation.getBouquets().get(i));
        }
        System.out.println("Choose bouquet to decorate:");
        int bouquetIndex = sc.nextInt() - 1;
        bouquet = bouquetOperation.getBouquets().get(bouquetIndex);

        for (int i = 0; i < DeliveryType.values().length; i++) {
            System.out.println("   " + (i + 1) + DeliveryType.values()[i]);
        }
        System.out.println("Choose you delivery:");
        int deliveryIndex = sc.nextInt() - 1;
        deliveryType = DeliveryType.values()[deliveryIndex];

        for (int i = 0; i < PackageType.values().length; i++) {
            System.out.println("   " + (i + 1) + PackageType.values()[i]);
        }
        System.out.println("Choose you package:");
        int packageIndex = sc.nextInt() - 1;
        packageType = PackageType.values()[packageIndex];

        BouquetDecorator deliveryDecorator = new Delivery(deliveryType);
        BouquetDecorator packageDecorator = new Package(packageType);

        deliveryDecorator.setBouquet(bouquet);
        packageDecorator.setBouquet(deliveryDecorator);
    }

    private void orderState() {
        System.out.println("Order:");
        for (int i = 0; i < bouquetOperation.getBouquets().size(); i++) {
            System.out.println("   " + (i +1) + bouquetOperation.getBouquets().get(i));
        }
        System.out.println("Status" + bouquetOperation.getOrder().getState());
        System.out.println("Paid? y/n");
        String paid = sc.nextLine();
        if (paid.toLowerCase().equals("y")) {
            bouquetOperation.getOrder().paid();
        }
    }

    /**
     * Creating menu.
     */
    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Catalog of bouquet");
        menu.put("2", "2 - Choose custom bouquet");
        menu.put("3", "3 - Decorate bouquet");
        menu.put("4", "4 - Choose package, delivery, discount");
        menu.put("5", "5 - Order state");
        menu.put("Q", "Q - Exit");
    }

    /**
     * Show menu in console.
     */
    private void getMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet()) {
            System.out.println(menu.get(key));
        }
    }

    /**
     * Main method to work with user in console.
     */
    private void show() {
        String keyMenu;
        do {
            getMenu();
            System.out.println("Please, select menu point.");
            keyMenu = sc.nextLine().toUpperCase();
            try {
                methods.get(keyMenu).print();
            } catch (Exception e) {
                log.info(e);
            }
        } while (!keyMenu.equals("Q"));
    }

}