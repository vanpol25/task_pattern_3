package com.polahniuk.model.bouquet;

import com.polahniuk.model.flower.Color;

public enum PackageType {

    DEFAULT_WRAPER,
    PAPER_WRAPER,
    PLASTIC_WRAPER,
    BOX,
    BASKET;

    private Color color = Color.WHITE;

    public void setColor(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

}
