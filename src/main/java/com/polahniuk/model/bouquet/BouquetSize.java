package com.polahniuk.model.bouquet;

public enum BouquetSize {

    S, M, L

}
