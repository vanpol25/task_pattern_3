package com.polahniuk.model.bouquet;

public enum BouquetType {

    CUSTOM,
    BIRTHDAY,
    FUNERAL,
    VALENTINE,
    WEDDING

}
