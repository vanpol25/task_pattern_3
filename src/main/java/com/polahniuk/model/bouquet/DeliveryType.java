package com.polahniuk.model.bouquet;

public enum  DeliveryType {

    DEFAULT_DELIVERY,
    ONE_HOUR_DELIVERY,
    THIS_DAY_DELIVERY,
    NEXT_DAY_DELIVERY

}
