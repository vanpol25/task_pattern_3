package com.polahniuk.model.bouquet.factory.impl;

import com.polahniuk.model.bouquet.Bouquet;
import com.polahniuk.model.bouquet.BouquetSize;
import com.polahniuk.model.bouquet.BouquetType;
import com.polahniuk.model.bouquet.factory.BouquetFactory;
import com.polahniuk.model.bouquet.impl.BouquetImpl;
import com.polahniuk.model.flower.Color;
import com.polahniuk.model.flower.impl.Chrysanthemum;
import com.polahniuk.model.flower.impl.Rose;

public class FuneralBouquet extends BouquetFactory {

    private final BouquetType type = BouquetType.FUNERAL;

    public BouquetType getType() {
        return type;
    }

    @Override
    protected Bouquet createBouquet(BouquetSize size) {
        Bouquet bouquet;
        switch (size) {
            case S: {
                bouquet = new BouquetImpl(type);
                bouquet.addFlowers(new Rose(Color.RED), 2);
                bouquet.addFlowers(new Rose(Color.WHITE), 2);
                bouquet.addFlowers(new Chrysanthemum(Color.VIOLET), 2);
                break;
            }
            case M: {
                bouquet = new BouquetImpl(type);
                bouquet.addFlowers(new Rose(Color.RED), 4);
                bouquet.addFlowers(new Rose(Color.WHITE), 4);
                bouquet.addFlowers(new Chrysanthemum(Color.VIOLET), 4);
                break;
            }
            case L: {
                bouquet = new BouquetImpl(type);
                bouquet.addFlowers(new Rose(Color.RED), 8);
                bouquet.addFlowers(new Rose(Color.WHITE), 8);
                bouquet.addFlowers(new Chrysanthemum(Color.VIOLET), 8);
                break;
            }
            default: {
                bouquet = null;
                break;
            }
        }
        return bouquet;
    }
}
