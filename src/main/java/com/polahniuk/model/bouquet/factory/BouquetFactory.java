package com.polahniuk.model.bouquet.factory;

import com.polahniuk.model.bouquet.Bouquet;
import com.polahniuk.model.bouquet.BouquetSize;

public abstract class BouquetFactory {

    protected abstract Bouquet createBouquet(BouquetSize size);

    public Bouquet create(BouquetSize size) {
        Bouquet bouquet = createBouquet(size);
        return bouquet;
    }

}
