package com.polahniuk.model.bouquet.factory.impl;

import com.polahniuk.model.bouquet.Bouquet;
import com.polahniuk.model.bouquet.BouquetSize;
import com.polahniuk.model.bouquet.BouquetType;
import com.polahniuk.model.bouquet.factory.BouquetFactory;
import com.polahniuk.model.bouquet.impl.BouquetImpl;
import com.polahniuk.model.flower.Color;
import com.polahniuk.model.flower.impl.Rose;

public class WeddingBouquet extends BouquetFactory {

    private final BouquetType type = BouquetType.WEDDING;

    public BouquetType getType() {
        return type;
    }

    @Override
    protected Bouquet createBouquet(BouquetSize size) {
        Bouquet bouquet;
        switch (size) {
            case S: {
                bouquet = new BouquetImpl(type);
                bouquet.addFlowers(new Rose(Color.RED), 3);
                bouquet.addFlowers(new Rose(Color.WHITE), 3);
                bouquet.addFlowers(new Rose(Color.VIOLET), 3);
                break;
            }
            case M: {
                bouquet = new BouquetImpl(type);
                bouquet.addFlowers(new Rose(Color.RED), 5);
                bouquet.addFlowers(new Rose(Color.WHITE), 5);
                bouquet.addFlowers(new Rose(Color.VIOLET), 5);
                break;
            }
            case L: {
                bouquet = new BouquetImpl(type);
                bouquet.addFlowers(new Rose(Color.RED), 9);
                bouquet.addFlowers(new Rose(Color.WHITE), 9);
                bouquet.addFlowers(new Rose(Color.VIOLET), 9);
                break;
            }
            default: {
                bouquet = null;
                break;
            }
        }
        return bouquet;
    }
}
