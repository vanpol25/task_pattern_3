package com.polahniuk.model.bouquet.factory.impl;

import com.polahniuk.model.flower.Color;
import com.polahniuk.model.bouquet.Bouquet;
import com.polahniuk.model.bouquet.BouquetSize;
import com.polahniuk.model.bouquet.BouquetType;
import com.polahniuk.model.bouquet.factory.BouquetFactory;
import com.polahniuk.model.bouquet.impl.BouquetImpl;
import com.polahniuk.model.flower.impl.Camomile;
import com.polahniuk.model.flower.impl.Chrysanthemum;

public class BirthdayBouquet extends BouquetFactory {

    private final BouquetType type = BouquetType.BIRTHDAY;

    @Override
    protected Bouquet createBouquet(BouquetSize size) {
        Bouquet bouquet;
        switch (size) {
            case S: {
                bouquet = new BouquetImpl(type);
                bouquet.addFlowers(new Camomile(Color.BLUE), 3);
                bouquet.addFlowers(new Camomile(Color.WHITE), 3);
                bouquet.addFlowers(new Chrysanthemum(Color.VIOLET), 3);
                break;
            }
            case M: {
                bouquet = new BouquetImpl(type);
                bouquet.addFlowers(new Camomile(Color.BLUE), 5);
                bouquet.addFlowers(new Camomile(Color.WHITE), 5);
                bouquet.addFlowers(new Chrysanthemum(Color.VIOLET), 5);
                break;
            }
            case L: {
                bouquet = new BouquetImpl(type);
                bouquet.addFlowers(new Camomile(Color.BLUE), 9);
                bouquet.addFlowers(new Camomile(Color.WHITE), 9);
                bouquet.addFlowers(new Chrysanthemum(Color.VIOLET), 9);
                break;
            }
            default: {
                bouquet = null;
                break;
            }
        }
        return bouquet;
    }
}
