package com.polahniuk.model.bouquet;

import com.polahniuk.model.flower.Flower;

public interface Bouquet {

    void decorate();

    void addFlowers(Flower flower, int quantity);

    PackageType getPackageType();

    DeliveryType getDeliveryType();

    int getPrice();

}
