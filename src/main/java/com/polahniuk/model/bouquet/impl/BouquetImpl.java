package com.polahniuk.model.bouquet.impl;

import com.polahniuk.model.bouquet.Bouquet;
import com.polahniuk.model.bouquet.BouquetType;
import com.polahniuk.model.bouquet.DeliveryType;
import com.polahniuk.model.bouquet.PackageType;
import com.polahniuk.model.flower.Flower;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class BouquetImpl implements Bouquet {

    private Logger log = LogManager.getLogger(BouquetImpl.class);
    private List<Flower> bouquet = new ArrayList<>();
    private BouquetType type;
    private DeliveryType deliveryType;
    private PackageType packageType;

    public BouquetImpl(BouquetType type) {
        this.type = type;
        deliveryType = DeliveryType.DEFAULT_DELIVERY;
        packageType = PackageType.DEFAULT_WRAPER;
    }

    public int getPrice() {
        int price = 0;
        for (Flower flower : bouquet) {
            price += flower.getPrice();
        }
        return price;
    }

    @Override
    public DeliveryType getDeliveryType() {
        return deliveryType;
    }


    public void setDeliveryType(DeliveryType deliveryType) {
        this.deliveryType = deliveryType;
    }

    @Override
    public PackageType getPackageType() {
        return packageType;
    }

    public void setPackageType(PackageType packageType) {
        this.packageType = packageType;
    }

    @Override
    public void addFlowers(Flower flower, int quantity) {
        for (int i = 0; i < quantity; i++) {
            try {
                bouquet.add((Flower) flower.clone());
            } catch (CloneNotSupportedException e) {
                log.info(e);
            }
        }
    }

    @Override
    public void decorate() {

    }

    @Override
    public String toString() {
        return "Bouquet=" + bouquet +
                ", type=" + type +
                ", price=" + getPrice();
    }
}
