package com.polahniuk.model.bouquet.state.impl;

import com.polahniuk.model.bouquet.state.State;

public class Paid implements State {

    @Override
    public String toString() {
        return "Paid";
    }
}
