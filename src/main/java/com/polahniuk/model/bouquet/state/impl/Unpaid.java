package com.polahniuk.model.bouquet.state.impl;

import com.polahniuk.model.bouquet.state.Order;
import com.polahniuk.model.bouquet.state.State;

public class Unpaid implements State {

    @Override
    public void paid(Order order) {
        order.setState(new Paid());
    }

    @Override
    public String toString() {
        return "Unpaid";
    }
}
