package com.polahniuk.model.bouquet.state;

import com.polahniuk.model.bouquet.state.impl.Unpaid;

public class Order {

    private State state;


    public Order() {
        state = new Unpaid();
    }

    public void setState(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }

    public void paid() {
        state.paid(this);
    }

    public void unpaid() {
        state.paid(this);
    }
}
