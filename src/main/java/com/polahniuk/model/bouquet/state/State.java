package com.polahniuk.model.bouquet.state;

public interface State {

    default void paid(Order order){};
    default void unpaid(Order order){};

}
