package com.polahniuk.model.facade;

import com.polahniuk.model.bouquet.Bouquet;
import com.polahniuk.model.bouquet.BouquetSize;
import com.polahniuk.model.bouquet.BouquetType;
import com.polahniuk.model.bouquet.factory.BouquetFactory;
import com.polahniuk.model.bouquet.factory.impl.BirthdayBouquet;
import com.polahniuk.model.bouquet.factory.impl.FuneralBouquet;
import com.polahniuk.model.bouquet.factory.impl.ValentineBouquet;
import com.polahniuk.model.bouquet.factory.impl.WeddingBouquet;
import com.polahniuk.model.bouquet.state.Order;

import java.util.ArrayList;
import java.util.List;

public class BouquetOperation {

    private Order order;
    private List<Bouquet> bouquets;

    public BouquetOperation() {
        bouquets = new ArrayList<>();
        order = new Order();
    }

    public void addBouquet(int index, int size) {
        BouquetFactory factory;
        BouquetType bouquetType = BouquetType.values()[index];
        if (bouquetType == BouquetType.BIRTHDAY) {
            factory = new BirthdayBouquet();
        } else if (bouquetType == BouquetType.FUNERAL) {
            factory = new FuneralBouquet();
        } else if (bouquetType == BouquetType.VALENTINE) {
            factory = new ValentineBouquet();
        } else if (bouquetType == BouquetType.WEDDING) {
            factory = new WeddingBouquet();
        } else {
            factory = null;
        }
        BouquetSize bouquetSize = null;
        if (size == 1) {
            bouquetSize = BouquetSize.S;
        } else if (size == 2) {
            bouquetSize = BouquetSize.M;
        } else if (size == 3) {
            bouquetSize = BouquetSize.L;
        }
        Bouquet bouquet = factory.create(bouquetSize);
        bouquets.add(bouquet);
    }

    public List<Bouquet> getBouquets() {
        return bouquets;
    }

    public void addBouquet(Bouquet bouquet) {
        bouquets.add(bouquet);
    }

    public Order getOrder() {
        return order;
    }

}
