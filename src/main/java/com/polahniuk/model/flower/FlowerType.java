package com.polahniuk.model.flower;

public enum FlowerType {
    CAMOMILE,
    CHRYSANTHEMUM,
    ROSE,
    TULIP
}
