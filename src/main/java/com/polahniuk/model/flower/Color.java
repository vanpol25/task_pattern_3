package com.polahniuk.model.flower;

public enum Color {

    RED, WHITE, BLUE, YELLOW, VIOLET

}
