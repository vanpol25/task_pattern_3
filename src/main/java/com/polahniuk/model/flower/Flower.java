package com.polahniuk.model.flower;

public interface Flower extends Cloneable{

    Object clone() throws CloneNotSupportedException;

    int getPrice();

}
