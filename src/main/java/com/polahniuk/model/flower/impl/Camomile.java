package com.polahniuk.model.flower.impl;

import com.polahniuk.model.flower.Color;
import com.polahniuk.model.Property;
import com.polahniuk.model.flower.Flower;

public class Camomile implements Flower {

    private Color color;
    private int price = Property.getPrice("camomile");

    public Camomile(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "Camomile-" + color;
    }

}
