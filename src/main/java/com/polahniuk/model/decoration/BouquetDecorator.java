package com.polahniuk.model.decoration;

import com.polahniuk.model.bouquet.Bouquet;
import com.polahniuk.model.bouquet.DeliveryType;
import com.polahniuk.model.bouquet.PackageType;
import com.polahniuk.model.flower.Flower;

public class BouquetDecorator implements Bouquet {

    private Bouquet bouquet;
    private int additionalPrice;
    private DeliveryType deliveryType;
    private PackageType packageType;

    public void setAdditionalPrice(int additionalPrice) {
        this.additionalPrice = additionalPrice;
    }

    public DeliveryType getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(DeliveryType deliveryType) {
        this.deliveryType = deliveryType;
    }

    public PackageType getPackageType() {
        return packageType;
    }

    public void setPackageType(PackageType packageType) {
        this.packageType = packageType;
    }

    public void setBouquet(Bouquet bouquet) {
        this.bouquet = bouquet;
    }

    public Bouquet getBouquet() {
        return bouquet;
    }

    @Override
    public int getPrice() {
        return bouquet.getPrice() + additionalPrice;
    }

    @Override
    public void decorate() {

    }

    @Override
    public void addFlowers(Flower flower, int quantity) {

    }

}
