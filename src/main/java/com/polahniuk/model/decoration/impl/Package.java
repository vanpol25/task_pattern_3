package com.polahniuk.model.decoration.impl;

import com.polahniuk.model.Property;
import com.polahniuk.model.bouquet.Bouquet;
import com.polahniuk.model.bouquet.PackageType;
import com.polahniuk.model.decoration.BouquetDecorator;

public class Package extends BouquetDecorator {

    private int deliveryPrice;

    public Package(PackageType packageType) {
        if (packageType == null) {
            Bouquet bouquet = getBouquet();
            PackageType packageType1 = bouquet.getPackageType();
            deliveryPrice = setPackagePrice(getBouquet().getPackageType());
        } else {
            deliveryPrice = setPackagePrice(packageType);
            setPackagePrice(packageType);
        }
    }

    private int setPackagePrice(PackageType packageType) {
        return Property.getPrice(packageType.name());
    }

}
