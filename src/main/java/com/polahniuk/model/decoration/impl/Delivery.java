package com.polahniuk.model.decoration.impl;

import com.polahniuk.model.Property;
import com.polahniuk.model.bouquet.DeliveryType;
import com.polahniuk.model.decoration.BouquetDecorator;

public class Delivery extends BouquetDecorator {

    private int deliveryPrice;

    public Delivery(DeliveryType deliveryType) {
        if (deliveryType == null) {
            deliveryPrice = getDeliveryPrice(getBouquet().getDeliveryType());
        }else {
            deliveryPrice = getDeliveryPrice(deliveryType);
            setDeliveryType(deliveryType);
        }
    }

    public int getDeliveryPrice(DeliveryType deliveryType) {
        return Property.getPrice(deliveryType.name());
    }

}
