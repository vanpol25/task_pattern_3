package com.polahniuk.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Optional;
import java.util.Properties;

public class Property {

    private static Logger log = LogManager.getLogger(Property.class);
    private static Property instance;
    private static FileInputStream file;
    private static Properties properties;

    private Property() {
    }

    private static void connectToProp() {
        try {
            file = new FileInputStream(new File("C:\\Users\\Van_PC\\git\\task_pattern_3\\thirdPattern\\src\\main\\resources\\priceList.properties"));
            properties = new Properties();
            properties.load(file);
        } catch (FileNotFoundException e) {
            log.info(e);
        }catch (IOException e) {
            log.info(e);
        }
    }

    private static void createInstance() {
        if (instance == null) {
            instance = new Property();
            connectToProp();
        }
    }

    public static int getPrice(String key) {
        createInstance();
        Optional<String> price = Optional.of(properties.getProperty(key));
        return Integer.valueOf(price.orElseThrow(NullPointerException::new));
    }

}
