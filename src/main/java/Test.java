import com.polahniuk.model.bouquet.Bouquet;
import com.polahniuk.model.bouquet.BouquetSize;
import com.polahniuk.model.bouquet.DeliveryType;
import com.polahniuk.model.bouquet.PackageType;
import com.polahniuk.model.bouquet.factory.BouquetFactory;
import com.polahniuk.model.bouquet.factory.impl.BirthdayBouquet;
import com.polahniuk.model.decoration.BouquetDecorator;
import com.polahniuk.model.decoration.impl.Delivery;
import com.polahniuk.model.decoration.impl.Package;

public class Test {
    public static void main(String[] args) {
        BouquetFactory factory = new BirthdayBouquet();
        Bouquet bouquet = factory.create(BouquetSize.S);
        System.out.println(bouquet);

        BouquetDecorator devileryDecorator = new Delivery(DeliveryType.ONE_HOUR_DELIVERY);
        BouquetDecorator packageDecorator = new Package(PackageType.DEFAULT_WRAPER);

        devileryDecorator.setBouquet(bouquet);
        packageDecorator.setBouquet(devileryDecorator);

        BouquetDecorator finalBouquet = packageDecorator;

        int price = finalBouquet.getPrice();

        System.out.println(price);

    }
}
